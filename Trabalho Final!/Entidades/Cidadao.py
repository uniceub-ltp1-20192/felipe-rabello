class Cidadao:
  
    def __init__(self,tamanho=0,nome="",cpf=0,peso=0,identificador = 0):
        self._tamanho       = tamanho
        self._nome          = nome
        self._cpf           = cpf
        self._peso          = peso
        self._identificador = identificador

    @property
    def identificador(self):
        return self._identificador

    @identificador.setter
    def identificador(self,identificador):
        self._identificador = identificador

    @property
    def tamanho(self):
        return self._tamanho

    @tamanho.setter
    def tamanho(self,tamanho):
        self._tamanho = tamanho

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self,nome):
        self._nome = nome

    @property
    def cpf(self):
        return self._cpf

    @cpf.setter
    def cpf(self,cpf):
        self._cpf = cpf

    @property
    def peso(self):
        return self._peso

    @peso.setter
    def peso(self,peso):
        self._peso = peso

    def sotaque(self):
        print('Exxcada')

    def cozinhar(self):
        print('Lagoxxta')
