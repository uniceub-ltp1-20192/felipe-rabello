from Entidades.carioca import Carioca

class Dados:

    def __init__(self):
        self._dados = dict()
        self.identificador = 0
        
    def buscarPorIdentificador(self,paramIdentificador):
        ''' len() retorna tamanho do dicionario/array'''
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            return self._dados.get(int(paramIdentificador))

    def buscarPorAtributo(self,param):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            '''o metodo values() do dicionario
            retorna cada valor incluso no dicionario
             '''
            x = Carioca()
            while x in self._dados.values():
                if x.tamanho == param:
                    return x
            return None

    def inserir(self,entidade):
        '''Gerando o proximo identificador e colocando
        no atributo identificador da entidade'''
        entidade.identificador = self.gerarProximoIdentificador()
        '''Salva a entidade dentro de uma posicao do
        dicionario utilizando o identificador'''
        self._dados[entidade.identificador] = entidade                  
    
    def deletar(self,entidade):
        del self._dados[entidade.identificador]


    def gerarProximoIdentificador(self):
        '''incrementa +1 ao valor de identificador'''
        self.identificador = self.identificador + 1
        return self.identificador
    def alterar(self,entidade):
        self._dados[entidade.identificador] = entidade 
