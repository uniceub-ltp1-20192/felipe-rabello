convidados = ["Neymar Jr", "Lionel Messi", "Cristiano Ronaldo"]
convidados[2] = "Hazard"
convidados.insert(0, "Vinícius Jr")
convidados.insert(2, "Salah")
convidados.append("Ter Stegen")
desmarcados = ["Cristiano Ronaldo"]


def mensagemconvite():
    for c in range (0,6):
        print("É com muita honra e felicidade que eu convido o jogador, {}, para um jantar de celebração.\n".format(convidados[c]))
    return


def mensagem_ausencia():
    print("\nInfelizmente o convidado, {}, não poderá comparecer no jantar!\n".format(desmarcados[0]))
    return


mensagemconvite()
mensagem_ausencia()

print("\n\nHoje é nosso dia de sorte, encontramos uma mesa maior e teremos mais convidados!\n\n")