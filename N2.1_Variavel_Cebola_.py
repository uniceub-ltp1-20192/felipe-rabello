cebolas = 300
##variavel para indicar a quantidade de cebola

cebolas_na_caixa = 120
##variavel para indicar a quantidade de cebolas em uma caixa

espaco_caixa = 5
##variavel para indicar a quantidade de esoaços da caixa

caixas = 60
##variavel de numeros de caixas

cebolas_fora_da_caixa = int(cebolas - cebolas_na_caixa)
##variavel que indicar a quantidade de cebolas que não estão na caixa

caixas_vazias = int (caixas - (cebolas_na_caixa / espaco_caixa))
##variavel para indicar a quantidade de caixas vazias

caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
##variavel da operação matematica que indica a quantidade de caixas necessarias




print("Existem {} cebolas encaixotadas".format(cebolas_na_caixa))
##mensagem para o cliente mostrando a quantidade de cebolas na caixa
print("Existem {} cebolas sem caixa".format(cebolas_fora_da_caixa))
##mensagem para o cliente mostrando a quantidade de cebolas fora da caixa
print("Em cada caixa cabem {} cebolas".format(espaco_caixa))
##mensagem mostrando quantas cebolas cabem em cada caixa
print("Ainda temos {} caixas vazias".format(caixas_vazias))
##mensagem mostrando quantas caixas ainda estão disponiveis
print("Então precisamos de {} caixas para empacotar todas as cebolas".format(caixas_necessarias))
##mensagem indicando quantas caixas ainda são necessárias